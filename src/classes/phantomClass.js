import phantomjs from 'phantomjs-prebuilt';

export default class {
    setFileName(myFileName){
        this.fileName = myFileName;
    }

    setSite(mySite) {
        this.site = mySite;
    }

    execute () {
        var program = phantomjs.exec('phantomjs-script.js', this.site, this.fileName);

        program.stdout.pipe(process.stdout);

        program.stderr.pipe(process.stderr);

        program.on('exit', code => {
            // do something on endc
        });

        program.on('error', (msg, trace) => {
            var msgStack = ['ERROR: ' + msg];
            if (trace && trace.length) {
                msgStack.push('TRACE:');
                trace.forEach(function(t) {
                    msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
                });
            }
            console.error(msgStack.join('\n'));
        });
    }
}