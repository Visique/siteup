var system = require('system');

var args = system.args;
var page = require('webpage').create();

page.open(args[1], function() {
    var fileName = 'site.png';

    if(args[2] !== undefined) {
        fileName = args[2];
    }

    page.render('sites/' + fileName);
    phantom.exit();
});