import webpack from 'webpack';

export default {
    entry: './src/app.js',
    output: {
        filename: './src/bundle.js'
    },
    loaders: [
        { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
    ]
}