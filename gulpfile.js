var gulp = require('gulp');
var babel = require('gulp-babel');
var webpack = require('gulp-webpack');

gulp.task('default', () => {
    var paths = ['src/**/*.js'];
    return gulp.src(paths)
        .pipe(babel())
        /*.pipe(webpack({
            module: {
                loaders: [
                    { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
                ]
            }
        }))*/
        .pipe(gulp.dest('dist'));

});